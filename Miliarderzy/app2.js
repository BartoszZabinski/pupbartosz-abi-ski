var questions = [
    {
        'question' : 'Ile Hubert ma pytań?',
        'answers' : ['6','16','26','73'],
        'correctAnswer' : '16'
    },
    {
        'question' : 'Jak ma na imie Monika Brodka?',
        'answers' : ['Monika','Ania','Janusz','Kunegunda'],
        'correctAnswer' : 'Janusz' 
    },
    {
        'question' : 'Która jest godzina?',
        'answers' : ['W pół do komina','9','11','16'],
        'correctAnswer' : '9'
    },
];
var usedIndexes = [];
var currentQuestionNum = selectQuestionNum(usedIndexes);
var currentQuestion = questions[currentQuestionNum];

function selectQuestionNum(){
    var q = Math.floor(Math.random() * questions.length);
    if(usedIndexes.length < questions.length){
        while(usedIndexes.indexOf(q) >= 0){
            q = Math.floor(Math.random()* questions.length)
        }
        usedIndexes.push(q);   
    } 
    else {
        q = -1;
    }
    return q;
}

function fillData(question){
    var questionBox = document.getElementById('question'); 
    var answersElems = document.querySelectorAll('button');
    questionBox.innerHTML = question.question;
    
    for(var i = 0; i < answersElems.length; i++){
        answersElems[i].innerHTML = question.answers[i];
    }
}


function addListeners() {
    var btns = document.querySelectorAll('button');
    
    for(var i=0; i < btns.length; i++){
        btns[i].addEventListener('click',function(e) {
            if(e.target.innerHTML == currentQuestion.correctAnswer) {
                currentQuestionNum = selectQuestionNum();
                currentQuestion = questions[currentQuestionNum];
                fillData(currentQuestion)
                
            } else {
                alert('KONIEC GRY!');
            }
        });
    }
}

addListeners();
fillData(currentQuestion);

