var Stack = function() { 
	
	var elems = [];
	
	function empty() {
		return (elems.length == 0) ? true : false;
	}
	function push(arg) {
		elems.push(arg);
	}
	function pop() {
		return (elems.length === 0) ? null : elems.pop();
	}
	function size() {
		return elems.length;
	}
	function top() {
		return (elems.length === 0) ? null : elems.length-1;
	}
	
	return {
		empty : empty,
		push : push,
		pop : pop,
		top : top,
		size : size,
	}
	
};

var myStack = new Stack();
	console.log(myStack.pop());
	console.log(myStack.size());
	console.log(myStack.push(5));
	console.log(myStack.push(9));
	console.log(myStack.top());
	console.log(myStack.size());
	console.log(myStack.pop());
	console.log(myStack.empty());
	console.log(Stack());
