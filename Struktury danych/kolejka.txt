function Queue(s) {
    this.elems = [],
    this.head = null;
    this.tail = 0;
    
    this.maxSize = s || 100;
}

Queue.prototype.empty = function() {
    return (this.elems.length === 0) ? true : false;
};
Queue.prototype.push = function(arg) {
    this.tail = (this.elems.length === 0) ? null : this.elems.length;
    this.head = (this.elems.length === 0) ? null : 0;
    this.elems.push(arg);
};
Queue.prototype.pop = function() {
    this.tail = (this.elems.length === 0) ? null : this.elems.length;
    this.head = (this.elems.length === 0) ? null : 0;
    if (this.elems.length === 0) return null;
            else return this.elems.shift();
};
Queue.prototype.size = function() {
    if (this.elems.length === 0) return null;
            else return this.elems.length;
};

var myQueue = new Queue();
console.log(myQueue.empty());
myQueue.push(3);
myQueue.push(7);
myQueue.push(3);
myQueue.push(7);

console.log(myQueue.empty());
console.log(myQueue.size());

console.log(myQueue.head);
console.log(myQueue.tail);
