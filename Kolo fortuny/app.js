// Naszym kolejnym zadaniem będzie napisanie gry przeglądarkowej do zgadywania haseł. System losuje 1 słowo z zadeklarowanych, a następnie wyświetla słowo zakodowane w sposób: _ _ _ _
// Po prawej stronie znajdywać będą się litery, wygenerowane skryptowo (tj. umieszone automatycznie), po wciśnięciu których nastąpi odsłona litery w słowie, jeśli taka istnieje. Dodatkowo, jeżeli użytkownik wciśnie na klawiaturze wybrany klawisz, litera też powinna się odsłonić. Jeżeli litera została wciśnięta, przycisk powinien zostać zablokowany tak, aby nie można było go ponownie użyć dla wybranego słowa.
// Powyżej listy liter do wciśnięcia znajdywać się powinna lista użytych już liter.
// Liczba prób zgadywania: 2 * długość_słowa,
// Dodatkowo użytkownik ma możliwość 3 razy zgadnięcia hasła w inpucie, który znajduje się nad listą zgadywanych wyrazó.
// UWAGA! Przyjmij, że w strukturze html znajduje się tylko i wyłącznie (w sekcji body) <div class="fortune"></div>, reszta elementów tworzona jest przez skrypt.



$(document).ready(function() {
    /* przyklad */
    
    $('<div/>', { class : 'leftSide'}).prependTo('.fortune');
    $('<div/>', { class : 'rightSide'}).appendTo('.fortune');
    $('<div/>', { class : 'clear'}).appendTo('.fortune');
    $('<div/>', { class : 'guessBox'}).prependTo('.rightSide');
    $('<div/>', { class : 'counter'}).html('Ilość prób: <span></span>').appendTo('.rightSide');
    $('<div/>', { class : 'usedLetterBox'}).html('Uzyte litery: <span class="usedLetters"></span>').appendTo('.rightSide');
    $('<div/>', { class : 'letterBox'}).appendTo('.rightSide');
    $('<input/>', { name : 'guess', id : 'guess', type : 'text'}).appendTo('.guessBox');
    $('<button/>', { id : 'guessBtn'}).html('Zgaduj').appendTo('.guessBox');
    $('<h1/>', { class : 'title'}).html('Koło fortuny').prependTo('.leftSide');
    $('<div/>', { class : 'phrase' }).html('_______').appendTo('.leftSide');
    
    for (var i = 65; i < 91; i++) {
        $('<button/>', {id : String.fromCharCode(i), class : 'guessLetter'}).html(String.fromCharCode(i)).appendTo('.letterBox');
    }
    
    /*deklaracja slow*/
    var words = ['picie', 'to', 'zycie', 'robota', 'glupota'],
        currentWord = words[(Math.random()*words.length | 0)].toUpperCase(),
        dashedWord = "_".repeat(currentWord.length);
    /*umieszczamy zakodowane slowo*/
    $('.phrase').html(dashedWord);
    /*ustawiamy ilosc prob*/
    $('.counter').find('span').html(currentWord.length*2)
    function guessLetterGame(e){
        
        var splitedDashedWord = dashedWord.split('');
        var currentLetter = (e.type == 'keyup') ? String.fromCharCode(e.keyCode) : $(this).attr('id');
        var $counter =  $('.counter').find('span');
        var currentCounter = parseInt($counter.html());
        var $usedLetters = $('.usedLetters');
        var letterExist = false;

        if(e.target.tagName == 'INPUT') {
            return;
        }
        
        for (var i = 0; i < dashedWord.length; i++) {
            if (currentWord.charAt(i) == currentLetter) {
                splitedDashedWord[i] = currentLetter;
                letterExist = true;
                
            }
        }
        dashedWord = splitedDashedWord.join('');
        $('.phrase').html(dashedWord);
        $counter.html(currentCounter-1);
        if ($usedLetters.html().length == 0) {
            $usedLetters.html(currentLetter);
        } else {
            $usedLetters.append(', ' + currentLetter);
        }
        /*wyszarzenie przycisku po wcisnieciu i zblokowanie*/
        $('#' + currentLetter).attr('disabled', 'disabled');
        /*ustawienie ramki na przycisku*/
        if(letterExist) {
           $('#' + currentLetter).addClass('rightLetter'); 
        } else {
           $('#' + currentLetter).addClass('wrongLetter'); 
        }
        /*sprawdzenie wygranej*/
        if(checkForWin(dashedWord)) {
            return;
        }
        /*sprawdzenie przegranej*/
        if(currentCounter == 1) {
            $('.guessLetter').off('click');
            $(document).off('keyup');
            $('.phrase').html('Przegrana (' + currentWord + ')');
            return;
        }
    }
    
    function checkForWin(word) {
        if(word == currentWord) {
            $('.guessLetter').off('click');
            $(document).off('keyup');
            $('.phrase').html('Wygrana (' + currentWord + ')').addClass('greenColor');
            return true; 
        }
        return false;
    }
    
    $('.guessLetter').on('click', guessLetterGame);
    $(document).on('keyup', guessLetterGame);
    $('#guessBtn').on('click',function() {
        checkForWin($('#guess').val().toUpperCase());
        
    });

    
    //$('<button/>', {id : 'A', class : 'guessLetter'}).html('A').appendTo('.letterBox');
    
    /*
    $(window).on('keyup', function(e) {
		console.log(String.fromCharCode(e.keyCode));
	});*/
});

