1. Napisz funkcj� zwracaj�c� warto�� bezwzgl�dn� liczby. 

function liczba(a) { 
     return (a < 0)? -a : a; 
} 
 
console.log('Wartosc bezwzgledna podanej liczby to: ' + liczba(-21)); 

 

2. Napisz funkcj�, kt�ra dla zadanego dnia wy�wietli jego dzie� tygodnia (np. 1 -niedziela).  

(nale�y zbada� czy liczba mie�ci si� w zakresie). 

function dzien(a){ 
    switch(a){ 
        case 1: 
            return 'poniedzialek'; 
        case 2: 
            return 'wtorek'; 
        case 3: 
            return 'sroda'; 
        case 4: 
            return 'czwartek'; 
        case 5: 
            return 'piatek'; 
        case 6: 
            return 'sobota'; 
        case 7: 
            return 'niedziela'; 
        default: 
            return 'podales nieprawidlowy nr. dnia'; 
    } 
} 
 
console.log(dzien(1)); 

 

3. Napisz funkcj�, kt�ra zwr�ci nazw� miesi�ca dla przekazanej liczby (nale�y zbada� czy liczba  

mie�ci si� w zakresie). 

function miesiac(a){ 
    switch(a){ 
        case 1: 
            return 'styczen'; 
        case 2: 
            return 'luty'; 
        case 3: 
            return 'marzec'; 
        case 4: 
            return 'kwiecien'; 
        case 5: 
            return 'maj'; 
        case 6: 
            return 'czerwiec'; 
        case 7: 
            return 'lipiec'; 
        case 8: 
            return 'sierpien'; 
        case 9: 
            return 'wrzesien'; 
        case 10: 
            return 'pazdziernik'; 
        case 11: 
            return 'listopad'; 
        case 12: 
            return 'grudzien'; 
        default: 
            return 'podales nieprawidlowy nr. miesiaca'; 
    } 
} 
 
console.log(miesiac(3)); 

4. Napisz funkcj� licz�ca NWD wykorzystuj�c algorytm Euklidesa.

function nwd(a,b) {
	while (a != b) {
		if (a > b) {
			a -= b;
		}
		if (b > a) {
			b -= a;
		}
	}
	return (a);
}

console.log(nwd(121, 44));

5. Napisz funkcj�, kt�ra przyjmuje jako argumenty: g -godziny, m -minuty, s -sekundy, 
a nast�pnie zwraca podany czas w sekundach.

function time(g, m, s){
	
	
	var suma = g*3600 + m*60 + s;

	return suma;

}



console.log(time(1,20,7));

6. U�ytkownik podaje dwie liczby ca�kowite a, b. algorytm ma za zadanie wypisa� wszystkie 
parzyste liczby w kolejno�ci rosn�cej, a nast�pnie wszystkie liczby nieparzyste w kolejno�ci malej�cej z przedzia�u
<a;b>. niech a, b �liczby ca�kowite z zakresu 0-255.
Np. dla danych wej�ciowych a=3, b=8,otrzymujemy plik wynikowy: 4, 6, 8, 7, 5, 3.

function liczby(a, b){
	
	if(a<0 || a>255 || b<0 || b>255 || b<a){
		console.log('podano niewlasciwe liczby');
	}
	
	var nums = [];
	
	if(a >=0 && a<=255 && b >=0 && b<=255 && a<b){
		for(var i=a; i<=b; i++){
		if(i % 2 == 0){
			nums.push(i);
		}	
	}
	
	for(var i=b; i>=a; i--)
	{
		if(i % 2 != 0){
			nums.push(i);
		}
	}
	return nums;
	}
}

console.log(liczby(1,8));

7.Napisz funkcj�, kt�ra dla zadanej liczby zwr�ci sum� kwadrat�w poszczeg�lnych liczb od 1 do 
zadanej liczby. Przyjmij i zbadaj czy u�ytkownik przekaza� liczb� w przedziale <0, 10>

function sqr(a){
	
	var suma = 0;

	
if(a < 0 || a > 10) {

		return 'poza zakresem';
	
	}
	
	
	for(var i = a; i >=0; i--){

		suma += i*i;

	}
	
	return suma;

}


console.log(sqr(9));


8. Napisz algorytm licz�cy ile potrzeba element�w (bloczk�w) dla piramidy o poziomie N 
(1 poziom 1 bloczek, 2 poziom, dwa bloczki itd.).

function pir(a) {


	var suma = 0;

	
for(i=1; i<=a; i++){

		suma += i;

	}

return suma;

}



console.log(pir(192));

 
9. Kasia ulokowa�a w banku pewna ilo�� z�otych na okres jednego roku.
Oprocentowanie roczne w tym banku wynosi 19,4%. Napisz algorytm, kt�ry b�dzie oblicza� ilo�� pieni�dzy na koncie  
po jednym roku dla dowolnej sumy pieni�dzy. Zmodyfikuj program tak, aby oblicza� kwot� dla wczytanej liczby lat.  

function bank(lata, kwota){
	
	var procent = 1.194,
	suma = kwota;
	var i= 1;
	
	while(i <= lata){
		suma = suma * procent;
		i++;
	}
	return suma;
	
}

console.log(bank(2,100));


10. Z Krakowa do Zakopanego jest 132 kilometry. Napisz algorytm, kt�ry b�dzie podawa� czas w 
jaki nale�y przeby� t� drog� przy r�nych pr�dko�ciach (zak�adamy, �e pojazd porusza si� ca�� 
drog� pr�dko�ci� jednostajn�.

function droga(v){

	var s = 132;

	t = s/v;

	return t;

}

function zmienCzas(t){

	var h = Math.floor(t);

	var m = (t - h) * 60;

	var s = (m - Math.floor(m)) * 60,

	ret = '';

	if (h>0) {

		ret += h + ' h ';

	}
	
	if (Math.floor(m) > 0) {

		ret += Math.floor(m) + ' m ';

	}
	
	if (Math.floor(s) > 0) {

		ret += Math.floor(s) + ' s ';

	}
	
	
	
	return ret ;


}



console.log('czas: ' + droga(140) + ' h' );


console.log(zmienCzas(droga(122)));


11. Dla zadanej tablicy element�w liczb ca�kowitych, napisz program, kt�ry poka�e r�nic� 
najwi�kszego oraz najmniejszego elementu, przyk�adowo, tablica zawiera elementy -5, 3, 2, 1, 10,
wynik powinien wynosi� 15, gdy� - 5 to najmniejszy element, a 10 to najwi�kszy.   

var tab = [-5, 3 , 2, 1, 10];



function calcDifference(tab) {

	
var min = tab[0];

	var max = tab[0];



	for(var i = 1; i < tab.length; i++){

		if (min > tab[i]) {

		
	min = tab[i];
	
		}

		if (max < tab[i]) {

		
	max = tab[i];

		}

	}	

	return max - min;

}



console.log(calcDifference(tab));


12. Na farmie wyst�puje n kr�lik�w. Nieparzyste z nich, maj� niestety tylko jedno ucho, parzyste 
s� w komplecie. Dla wczytanego n wypisz ile uszu znajduje si� na farmie.

function uszy(a) {

	
var liczbaUszu = 0;


	for(var i = 1; i <= a; i++){

		if(i % 2 === 0){

			liczbaUszu += 2;

		}
		
		else {

			liczbaUszu += 1;

		}

	}
	

return liczbaUszu;


}


console.log(uszy(86));    
 

13.Mamy dwie tablice liczb ca�kowitych, nale�y:  
	1. wy�wietli� te liczby, kt�re wyst�puj� w obydwu tablicach  
	2. wy�wietli� liczby z obu tablic, kt�re si� nie powtarzaj�


var tab1 = [5,6,7], tab2 = [6, 19, 9];

function numInArrays(arr1, arr2){
	getDiffNums(arr1, arr2);
	getDiffNums(arr1, arr2);
}



function getDiffNums(arr1, arr2) {
	for(var i = 0; i < arr1.length; i++){
	if(inArray(arr2, arr1[i])){
		console.log(arr1[i]);
	}
}
}

function inArray(haystack, needle) {
	for(var i = 0; i < haystack.length; i++) {
		if(haystack[i] == needle){
			return true;
		}
	}
return false;
}

console.log(numInArrays(tab1, tab2));




 